package com.example.hitanshu.contactsexample;

import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    List<Map<String, String>> contacts;
    ContactsAdapter contactsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recycler_view);
        contacts = new ArrayList<>();
        contactsAdapter = new ContactsAdapter(this, contacts);
        recyclerView.setAdapter(contactsAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        // RETRIEVE CONTACTS

        // bad way (slow)
//        Cursor cursor = getContentResolver().query(ContactsContract.Data.CONTENT_URI,
//                new String[]{
//                        ContactsContract.Data._ID,
//                        ContactsContract.Data.DISPLAY_NAME,
//                        ContactsContract.Data.HAS_PHONE_NUMBER,
//                        ContactsContract.CommonDataKinds.Phone.NUMBER
//                }, null, null, null);
//        while (cursor.moveToNext()) {
//            String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Data._ID));
//            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
//            String hasPhone = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.HAS_PHONE_NUMBER));
//            if (Integer.parseInt(hasPhone) > 0) {
//                Cursor phones = getContentResolver().query(ContactsContract.Data.CONTENT_URI,
//                        new String[]{
//                                ContactsContract.CommonDataKinds.Phone.NUMBER
//                        },
//                        ContactsContract.Data._ID + " = " + "\'" + contactId + "\'" + " AND " + ContactsContract.Data.MIMETYPE + " = " + "\'" + ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE + "\'",
//                        null, null);
//                while (phones.moveToNext()) {
//                    String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
//                    Map<String, String> map = new HashMap<>();
//                    map.put("name", name);
//                    map.put("number", phoneNumber);
//                    contacts.add(map);
//                }
//                phones.close();
//            }
//        }
//        cursor.close();

        // good way (fast)
        // (ideally it should NOT be done on UI thread)
        Cursor cursor = getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                new String[]{
                        ContactsContract.Data.DISPLAY_NAME,
                        ContactsContract.Data.HAS_PHONE_NUMBER,
                        ContactsContract.CommonDataKinds.Phone.NUMBER
                }, ContactsContract.Data.MIMETYPE + " = " + "\'" + ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE + "\'",
                null, ContactsContract.Data.DISPLAY_NAME + " ASC");
        while (cursor.moveToNext()) {
            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
            String hasPhone = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.HAS_PHONE_NUMBER));
            if (Integer.parseInt(hasPhone) > 0) {
                String phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                Map<String, String> map = new HashMap<>();
                map.put("name", name);
                map.put("number", phoneNumber);
                contacts.add(map);
            }
        }
        cursor.close();

        //UPDATE ADAPTER
        contactsAdapter.notifyDataSetChanged();
    }
}
